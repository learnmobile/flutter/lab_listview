import 'package:flutter/material.dart';

class CardUser extends StatelessWidget {
  final String sName;
  final String sCareer;
  final double average;
  final String uImage;

  const CardUser(this.sName, this.sCareer, this.average, this.uImage,
      {super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.blueAccent,
        border: Border.all(color: Colors.blueAccent),
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        boxShadow: const [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 4,
            offset: Offset(4, 8), // Shadow position
          ),
        ],
      ),
      child: Row(
        children: [
          Image(image: NetworkImage(uImage)),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                sName,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              Text(
                "Career: $sCareer",
                style: const TextStyle(fontSize: 18),
              ),
              Text(
                "Average: $average",
                style: const TextStyle(fontSize: 18),
              )
            ],
          ))
        ],
      ),
    );
  }
}
