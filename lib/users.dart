import 'package:flutter/material.dart';
import 'package:lab3_listview/card_user.dart';
import 'users_json.dart';

class Users extends StatefulWidget {
  const Users({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Users();
  }
}

class _Users extends State<Users> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('User List'),
              backgroundColor: Colors.indigo[900],
            ),
            body: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                child: ListView.builder(
                    itemCount: users['users'].length,
                    itemBuilder: (BuildContext context, int index) {
                      return CardUser(
                          "${users['users'][index]['fullname']}",
                          "${users['users'][index]['career']}",
                          double.parse("${users['users'][index]['average']}"),
                          "${users['users'][index]['image']}");
                    }))));
  }
}
