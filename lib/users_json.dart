Map users = {
  "users": [
    {
      "fullname": "Velma Hessel",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/342.jpg",
      "average": 3.49,
      "career": "Mechanical Engineering"
    },
    {
      "fullname": "Jackie Glover",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/56.jpg",
      "average": 2.19,
      "career": "Art & Design"
    },
    {
      "fullname": "Rafael Hermann",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/858.jpg",
      "average": 4.75,
      "career": "Art & Design"
    },
    {
      "fullname": "Elisa Herzog I",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1113.jpg",
      "average": 3.09,
      "career": "Architecture"
    },
    {
      "fullname": "Mable Stoltenberg",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/83.jpg",
      "average": 2.75,
      "career": "Architecture"
    },
    {
      "fullname": "Lucy Abernathy",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/412.jpg",
      "average": 2.60,
      "career": "Architecture"
    },
    {
      "fullname": "Dr. Marion Veum",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/997.jpg",
      "average": 0.12,
      "career": "Mechanical Engineering"
    },
    {
      "fullname": "Molly Kulas",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/340.jpg",
      "average": 3.26,
      "career": "Architecture"
    },
    {
      "fullname": "Micheal Boyer",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/21.jpg",
      "average": 4.22,
      "career": "Computer Science"
    },
    {
      "fullname": "Mrs. Kristie Marks PhD",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/461.jpg",
      "average": 2.44,
      "career": "Economics"
    },
    {
      "fullname": "Mamie Rutherford",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/220.jpg",
      "average": 2.71,
      "career": "Architecture"
    },
    {
      "fullname": "Miss Yvette Herman",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/159.jpg",
      "average": 1.66,
      "career": "Art & Design"
    },
    {
      "fullname": "Alexandra Stark",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/998.jpg",
      "average": 4.50,
      "career": "Architecture"
    },
    {
      "fullname": "Benny Wunsch V",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/560.jpg",
      "average": 2.91,
      "career": "Economics"
    },
    {
      "fullname": "Bernice Jones",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/666.jpg",
      "average": 3.47,
      "career": "Law"
    },
    {
      "fullname": "Chelsea Erdman",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/533.jpg",
      "average": 1.86,
      "career": "Accounting & Finance"
    },
    {
      "fullname": "Lucia Nolan",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/490.jpg",
      "average": 4.14,
      "career": "Computer Science"
    },
    {
      "fullname": "Velma McClure",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/564.jpg",
      "average": 1.20,
      "career": "Computer Science"
    },
    {
      "fullname": "Carolyn Rice",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/295.jpg",
      "average": 2.89,
      "career": "Mechanical Engineering"
    },
    {
      "fullname": "Clara Konopelski",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1190.jpg",
      "average": 4.07,
      "career": "Accounting & Finance"
    },
    {
      "fullname": "Dennis Barrows PhD",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/157.jpg",
      "average": 0.09,
      "career": "Mechanical Engineering"
    },
    {
      "fullname": "Amy Lemke",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/821.jpg",
      "average": 3.00,
      "career": "Architecture"
    },
    {
      "fullname": "Jacqueline Roob",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/467.jpg",
      "average": 2.12,
      "career": "Computer Science"
    },
    {
      "fullname": "Shaun Parker",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1062.jpg",
      "average": 0.45,
      "career": "Manufacturing Engineering"
    },
    {
      "fullname": "Marlon Volkman II",
      "image":
          "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/781.jpg",
      "average": 1.89,
      "career": "Art & Design"
    }
  ]
};
